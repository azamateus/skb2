import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2a', (req, res) => {
  const summ1 = (+req.query.a || 0) + (+req.query.b || 0)
  res.send(summ1.toString());
});

app.get('/task2b', (req, res) => {
  var fio1 ={};
  fio1.fio = "Invalid fullname";
  
  if (req.query.fullname.length>0) {
    var arrfio1 = req.query.fullname.split(" ");

    switch (arrfio1.length) {
      case 1:
        fio1.fam = arrfio1[0];
        fio1.fio = fio1.fam;
        break
      case 2:
        fio1.fam = arrfio1[1];
        fio1.name = arrfio1[0];
        fio1.fio = fio1.fam + " " + (fio1.name.slice(0,1)) +  ".";
        break
      case 3:
        fio1.fam = arrfio1[2];
        fio1.name = arrfio1[0];
        fio1.lastname = arrfio1[1];
        fio1.fio = fio1.fam + " " + (fio1.name.slice(0,1)) + ". " + (fio1.lastname.slice(0,1)) + ".";
        break
    }
  }
  //console.log("" + fio1.fio.toString());
  res.send(fio1.fio.toString());
});

app.get('/task2c', (req, res) => {
  const userName1=req.query.username;
  var userName2 ={};
  userName2.name = "Invalid username";
  var l = req.query.username.length;
  if (l>0) {
    userName2.name = "";
    var i;
    for (i=0; i<=l; i++) {
      if (userName1.slice(i,i+1).toString()=="/") {
        userName2.name="";
      } else {
        if (userName1.slice(i,i+1).toString()=="?") {
          break;
        } else if (userName1.slice(i,i+1).toString()=="@") {
            continue;
        } else {
          userName2.name=userName2.name + userName1.slice(i,i+1).toString();
        };
      };
    };
    userName2.name="@" + userName2.name;
  };
  res.send(userName2.name.toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
